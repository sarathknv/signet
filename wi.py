import os
import random
import time

import cv2
import numpy as np
import shutil
import torch
import torch.nn as nn
from torch import optim
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import Dataset
import torch.utils.data as data_utils
from sklearn import preprocessing
from torchvision import transforms
from skimage import filters, transform
from torch.nn import functional as F


def preprocess_signature(img,
                         canvas_size,
                         img_size=(170, 242),
                         input_size=(150, 220)) -> np.ndarray:
    """ Pre-process a signature image, centering it in a canvas, resizing the image and cropping it.
    Parameters
    ----------
    img : np.ndarray (H x W)
        The signature image
    canvas_size : tuple (H x W)
        The size of a canvas where the signature will be centered on.
        Should be larger than the signature.
    img_size : tuple (H x W)
        The size that will be used to resize (rescale) the signature
    input_size : tuple (H x W)
        The final size of the signature, obtained by croping the center of image.
        This is necessary in cases where data-augmentation is used, and the input
        to the neural network needs to have a slightly smaller size.
    Returns
    -------
    np.narray (input_size):
        The pre-processed image
    -------
    """
    img = img.astype(np.uint8)
    centered = normalize_image(img, canvas_size)
    centered = remove_background(centered)

    inverted = 255 - centered
    resized = resize_image(inverted, img_size)

    if input_size is not None and input_size != img_size:
        cropped = crop_center(resized, input_size)
    else:
        cropped = resized

    return cropped


def normalize_image(img,
                    canvas_size=(840, 1360)) -> np.ndarray:
    """ Centers an image in a pre-defined canvas size, and remove
    noise using OTSU's method.
    Parameters
    ----------
    img : np.ndarray (H x W)
        The image to be processed
    canvas_size : tuple (H x W)
        The desired canvas size
    Returns
    -------
    np.ndarray (H x W)
        The normalized image
    """

    # 1) Crop the image before getting the center of mass

    # Apply a gaussian filter on the image to remove small components
    # Note: this is only used to define the limits to crop the image
    blur_radius = 2
    blurred_image = filters.gaussian(img, blur_radius, preserve_range=True)

    # Binarize the image using OTSU's algorithm. This is used to find the center
    # of mass of the image, and find the threshold to remove background noise
    threshold = filters.threshold_otsu(img)

    # Find the center of mass
    binarized_image = blurred_image > threshold
    r, c = np.where(binarized_image == 0)
    r_center = int(r.mean() - r.min())
    c_center = int(c.mean() - c.min())

    # Crop the image with a tight box
    cropped = img[r.min(): r.max(), c.min(): c.max()]

    # 2) Center the image
    img_rows, img_cols = cropped.shape
    max_rows, max_cols = canvas_size

    r_start = max_rows // 2 - r_center
    c_start = max_cols // 2 - c_center

    # Make sure the new image does not go off bounds
    # Emit a warning if the image needs to be cropped, since we don't want this
    # for most cases (maybe ok for feature learning, so we don't raise an error)
    if img_rows > max_rows:
        # Case 1: image larger than required (height):  Crop.
        print('Warning: cropping image. The signature should be smaller than the canvas size')
        r_start = 0
        difference = img_rows - max_rows
        crop_start = difference // 2
        cropped = cropped[crop_start:crop_start + max_rows, :]
        img_rows = max_rows
    else:
        extra_r = (r_start + img_rows) - max_rows
        # Case 2: centering exactly would require a larger image. relax the centering of the image
        if extra_r > 0:
            r_start -= extra_r
        if r_start < 0:
            r_start = 0

    if img_cols > max_cols:
        # Case 3: image larger than required (width). Crop.
        print('Warning: cropping image. The signature should be smaller than the canvas size')
        c_start = 0
        difference = img_cols - max_cols
        crop_start = difference // 2
        cropped = cropped[:, crop_start:crop_start + max_cols]
        img_cols = max_cols
    else:
        # Case 4: centering exactly would require a larger image. relax the centering of the image
        extra_c = (c_start + img_cols) - max_cols
        if extra_c > 0:
            c_start -= extra_c
        if c_start < 0:
            c_start = 0

    normalized_image = np.ones((max_rows, max_cols), dtype=np.uint8) * 255
    # Add the image to the blank canvas
    normalized_image[r_start:r_start + img_rows, c_start:c_start + img_cols] = cropped

    # Remove noise - anything higher than the threshold. Note that the image is still grayscale
    normalized_image[normalized_image > threshold] = 255

    return normalized_image


def remove_background(img: np.ndarray) -> np.ndarray:
    """ Remove noise using OTSU's method.
        Parameters
        ----------
        img : np.ndarray
            The image to be processed
        Returns
        -------
        np.ndarray
            The image with background removed
        """

    img = img.astype(np.uint8)
    # Binarize the image using OTSU's algorithm. This is used to find the center
    # of mass of the image, and find the threshold to remove background noise
    threshold = filters.threshold_otsu(img)

    # Remove noise - anything higher than the threshold. Note that the image is still grayscale
    img[img > threshold] = 255
    return img


def resize_image(img,
                 size) -> np.ndarray:
    """ Crops an image to the desired size without stretching it.
    Parameters
    ----------
    img : np.ndarray (H x W)
        The image to be cropped
    size : tuple (H x W)
        The desired size
    Returns
    -------
    np.ndarray
        The cropped image
    """
    height, width = size

    # Check which dimension needs to be cropped
    # (assuming the new height-width ratio may not match the original size)
    width_ratio = float(img.shape[1]) / width
    height_ratio = float(img.shape[0]) / height
    if width_ratio > height_ratio:
        resize_height = height
        resize_width = int(round(img.shape[1] / height_ratio))
    else:
        resize_width = width
        resize_height = int(round(img.shape[0] / width_ratio))

    # Resize the image (will still be larger than new_size in one dimension)
    img = transform.resize(img, (resize_height, resize_width),
                           mode='constant', anti_aliasing=True, preserve_range=True)

    img = img.astype(np.uint8)

    # Crop to exactly the desired new_size, using the middle of the image:
    if width_ratio > height_ratio:
        start = int(round((resize_width-width)/2.0))
        return img[:, start:start + width]
    else:
        start = int(round((resize_height-height)/2.0))
        return img[start:start + height, :]


def crop_center(img,
                size) -> np.ndarray:
    """ Crops the center of an image
        Parameters
        ----------
        img : np.ndarray (H x W)
            The image to be cropped
        size: tuple (H x W)
            The desired size
        Returns
        -------
        np.ndarray
            The cRecentropped image
        """
    img_shape = img.shape
    start_y = (img_shape[0] - size[0]) // 2
    start_x = (img_shape[1] - size[1]) // 2
    cropped = img[start_y: start_y + size[0], start_x:start_x + size[1]]
    return cropped


def crop_center_multiple(imgs,
                         size) -> np.ndarray:
    """ Crops the center of multiple images
        Parameters
        ----------
        imgs : np.ndarray (N x C x H_old x W_old)
            The images to be cropped
        size: tuple (H x W)
            The desired size
        Returns
        -------
        np.ndarray (N x C x H x W)
            The cropped images
        """
    img_shape = imgs.shape[2:]
    start_y = (img_shape[0] - size[0]) // 2
    start_x = (img_shape[1] - size[1]) // 2
    cropped = imgs[:, :, start_y: start_y + size[0], start_x:start_x + size[1]]
    return cropped


class SigNet(nn.Module):
    def __init__(self, input_shape=(1, 150, 220), embedding_length=2048,
                 num_classes=30, forg=True):
        super(SigNet, self).__init__()
        self.in_channels = input_shape[0]
        self.embedding_length = embedding_length
        self.num_classes = num_classes
        self.forg = forg

        self.cnn = nn.Sequential(
                nn.Conv2d(self.in_channels, 96, kernel_size=11, stride=4, bias=False),
                nn.BatchNorm2d(96),
                nn.ReLU(inplace=True),

                nn.MaxPool2d(kernel_size=3, stride=2),

                nn.Conv2d(96, 256, kernel_size=5, stride=1, padding=2, bias=False),
                nn.BatchNorm2d(256),
                nn.ReLU(inplace=True),

                nn.MaxPool2d(kernel_size=3, stride=2),

                nn.Conv2d(256, 384, kernel_size=3, stride=1, padding=1, bias=False),
                nn.BatchNorm2d(384),
                nn.ReLU(inplace=True),

                nn.Conv2d(384, 384, kernel_size=3, stride=1, padding=1, bias=False),
                nn.BatchNorm2d(384),
                nn.ReLU(inplace=True),

                nn.Conv2d(384, 256, kernel_size=3, stride=1, padding=1, bias=False),
                nn.BatchNorm2d(256),
                nn.ReLU(inplace=True),

                nn.MaxPool2d(kernel_size=3, stride=2),
        )

        self.fc_emb = nn.Sequential(
                nn.Linear(256*3*5, self.embedding_length, bias=False),
                nn.BatchNorm1d(self.embedding_length),
                nn.ReLU(inplace=True),

                nn.Linear(self.embedding_length, self.embedding_length, bias=False),
                nn.BatchNorm1d(self.embedding_length),
                nn.ReLU(inplace=True),
        )

        self.fc_cls = nn.Linear(self.embedding_length, self.num_classes)

        self.fc_forg = nn.Linear(self.embedding_length, 1)

    def forward(self, x):
        x = self.cnn(x)
        x = x.view(x.size(0), -1)
        emb = self.fc_emb(x)

        cls = self.fc_cls(emb)
        forg = self.fc_forg(emb)
        return emb, cls, forg


# Data utilities
def get_train_test_subject_ids(total_subjects=50, development_proportion=0.7, exploitation_proportion=0.2,
                               validation_proportion=0.1):
    assert development_proportion + exploitation_proportion + validation_proportion <= 1

    num_development = int(total_subjects * development_proportion)
    num_exploitation = int(total_subjects * exploitation_proportion)
    num_validation = int(total_subjects * validation_proportion)
    total_ids = np.arange(1, total_subjects + 1, dtype=int)
    np.random.shuffle(total_ids)

    development_ids = total_ids[:num_development].copy()
    exploitation_ids = total_ids[num_development: num_development + num_exploitation].copy()
    validation_ids = total_ids[num_development + num_exploitation: num_development + num_exploitation + num_validation].copy()
    return development_ids, exploitation_ids, validation_ids


def create_development_dataset(data_dir, subject_ids, num_gen=24, num_forg=24,
                               train_proportion=0.9):
    le = preprocessing.LabelEncoder()
    le.fit(subject_ids)
    samples = []

    for idx in subject_ids:
        for i in range(1, num_gen+1):
            path = os.path.join(data_dir,
                                'full_org',
                                'original_%d_%d.png' % (idx, i))

            sample = {'path': path,
                      'label': le.transform([idx])[0],
                      'is_forgery': False
                      }
            samples.append(sample)

        for j in range(1, num_forg + 1):
            path = os.path.join(data_dir,
                                'full_forg',
                                'forgeries_%d_%d.png' % (idx, j))

            sample = {'path': path,
                      'label': le.transform([idx])[0],
                      'is_forgery': True
                      }
            samples.append(sample)

    random.shuffle(samples)

    num_train = int(train_proportion * len(samples))
    train_samples = samples[:num_train]
    val_samples = samples[num_train:]
    return train_samples, val_samples, le


class SignatureDataset(Dataset):
    def __init__(self, samples, transform=None):

        self.transform = transform
        self.samples = samples

    def __len__(self):
        return len(self.samples)

    def __getitem__(self, idx):

        sample = self.samples[idx]

        img_path = sample['path']
        img = cv2.imread(img_path, cv2.IMREAD_GRAYSCALE)
        img = preprocess_signature(img, canvas_size=(840, 1360),
                                   img_size=(170, 242), input_size=None)

        if self.transform is not None:
            img = self.transform(img)

        is_forgery = sample['is_forgery']
        label = sample['label']

        return {'image': img, 'label': label, 'is_forg': is_forgery}


def train_one_epoch(model, train_loader, optimizer, epoch, epochs,
                    loss_type, lamb, device, verbose=True):
    model.train()

    running_loss = 0
    classification_accuracy = 0.0
    forgery_accuracy = 0.0
    num_samples = 0
    num_samples_cls = 0

    for i, data in enumerate(train_loader):
        x = data['image'].float().to(device)
        y = data['label'].long().to(device)
        yforg = data['is_forg'].float().to(device)

        if loss_type == 'L1':
            # Eq (3) in https://arxiv.org/abs/1705.05787
            _, logits, forg_logits = model(x)
            forg_logits = forg_logits.squeeze()
            class_loss = F.cross_entropy(logits, y)
            forg_loss = F.binary_cross_entropy_with_logits(forg_logits, yforg)

            loss = (1 - lamb) * class_loss
            loss += lamb * forg_loss
        else:
            # Eq (4) in https://arxiv.org/abs/1705.05787
            _, logits, forg_logits = model(x)
            if torch.any(yforg == 0):
                class_loss = F.cross_entropy(logits[yforg == 0], y[yforg == 0])
            else:
                class_loss = 0

            forg_logits = forg_logits.squeeze()
            forg_loss = F.binary_cross_entropy_with_logits(forg_logits, yforg)

            loss = (1 - lamb) * class_loss
            loss += lamb * forg_loss

        optimizer.zero_grad()
        loss.backward()
        torch.nn.utils.clip_grad_value_(optimizer.param_groups[0]['params'], 10)

        optimizer.step()

        running_loss += loss.item() * y.size(0)
        pred = logits.argmax(1)
        if loss_type == 'L1':
            acc = y.eq(pred).float().sum()
            classification_accuracy += acc.item()
            num_samples_cls += y.size(0)
        else:
            if torch.any(yforg == 0):
                acc = y[yforg == 0].eq(pred[yforg == 0]).float().sum()
                classification_accuracy += acc.item()
                num_samples_cls += y[yforg == 0].size(0)
            else:
                classification_accuracy += 0
                num_samples_cls += 0

        num_samples += y.size(0)

        forg_pred = forg_logits > 0
        forg_acc = yforg.long().eq(forg_pred.long()).float().sum()
        forgery_accuracy += forg_acc.item()

        if verbose:
            print('Train [%2d/%2d]: [%4d/%4d] loss: %1.4f cls_acc: %.4f forg_acc: %.4f'
                  % (epoch+1, epochs, i+1, len(train_loader),
                     running_loss/num_samples,
                     classification_accuracy/num_samples_cls,
                     forgery_accuracy/num_samples), end="\r")
    return running_loss/num_samples, classification_accuracy/num_samples_cls, forgery_accuracy/num_samples


def test_one_epoch(model, test_loader, epoch, epochs,
                   loss_type, lamb, device, verbose=True):
    model.eval()
    running_loss = 0
    classification_accuracy = 0.0
    forgery_accuracy = 0.0
    num_samples = 0
    num_samples_cls = 0

    with torch.no_grad():
        for i, data in enumerate(test_loader):
            x = data['image'].float().to(device)
            y = data['label'].long().to(device)
            yforg = data['is_forg'].float().to(device)

            if loss_type == 'L1':
                # Eq (3) in https://arxiv.org/abs/1705.05787
                _, logits, forg_logits = model(x)
                forg_logits = forg_logits.squeeze()
                class_loss = F.cross_entropy(logits, y)
                forg_loss = F.binary_cross_entropy_with_logits(forg_logits, yforg)

                loss = (1 - lamb) * class_loss
                loss += lamb * forg_loss
            else:
                # Eq (4) in https://arxiv.org/abs/1705.05787
                _, logits, forg_logits = model(x)
                if torch.any(yforg == 0):
                    class_loss = F.cross_entropy(logits[yforg == 0], y[yforg == 0])
                else:
                    class_loss = 0

                forg_logits = forg_logits.squeeze()
                forg_loss = F.binary_cross_entropy_with_logits(forg_logits, yforg)

                loss = (1 - lamb) * class_loss
                loss += lamb * forg_loss

            running_loss += loss.item() * y.size(0)
            pred = logits.argmax(1)
            if loss_type == 'L1':
                acc = y.eq(pred).float().sum()
                classification_accuracy += acc.item()
                num_samples_cls += y.size(0)
            else:
                if torch.any(yforg == 0):
                    acc = y[yforg == 0].eq(pred[yforg == 0]).float().sum()
                    classification_accuracy += acc.item()
                    num_samples_cls += y[yforg == 0].size(0)
                else:
                    classification_accuracy += 0
                    num_samples_cls += 0

            num_samples += y.size(0)

            forg_pred = forg_logits > 0
            forg_acc = yforg.long().eq(forg_pred.long()).float().sum()
            forgery_accuracy += forg_acc.item()

            if verbose:
                print('Val [%2d/%2d]: [%4d/%4d] loss: %1.4f cls_acc: %.4f forg_acc: %.4f'
                      % (epoch + 1, epochs, i + 1, len(test_loader),
                         running_loss / num_samples,
                         classification_accuracy / num_samples_cls,
                         forgery_accuracy / num_samples), end="\r")
    return running_loss / num_samples, classification_accuracy / num_samples_cls, forgery_accuracy / num_samples


def save_checkpoint(state, checkpoint_path, best_checkpoint_name='best.pth.tar'):
    os.makedirs(os.path.dirname(checkpoint_path), exist_ok=True)
    torch.save(state, checkpoint_path)
    if state['is_best']:
        best_checkpoint_path = os.path.join(os.path.dirname(checkpoint_path),
                                            best_checkpoint_name)
        shutil.copyfile(checkpoint_path, best_checkpoint_path)


def main(args):
    np.random.seed(args.seed)

    if args.dataset.lower() == 'cedar':
        K = 55
        development_ids, exploitation_ids, validation_ids =\
            get_train_test_subject_ids(total_subjects=K)

        train_samples, val_samples, le = create_development_dataset(args.data_dir,
                                                                    development_ids,
                                                                    num_gen=24,
                                                                    num_forg=24,
                                                                    train_proportion=0.9)
    elif args.dataset.lower() == 'bengali':
        raise ValueError('Invalid dataset name')
    else:
        raise ValueError('Invalid dataset name')

    print('Dataset: ', args.dataset)
    print('Total subjects: ', K)
    # print('Train subjects: ', int(K * args.train_proportion))
    # print('Test subjects: ', K - int(K * args.train_proportion))
    print(' '*40)

    train_transforms = transforms.Compose([
        transforms.ToPILImage(),
        transforms.RandomCrop(args.input_size),
        transforms.ToTensor(),
    ])

    val_transforms = transforms.Compose([
        transforms.ToPILImage(),
        transforms.CenterCrop(args.input_size),
        transforms.ToTensor(),
    ])

    train_dataset = SignatureDataset(train_samples,
                                     train_transforms)
    val_dataset = SignatureDataset(val_samples,
                                   val_transforms)

    train_loader = data_utils.DataLoader(train_dataset,
                                         batch_size=args.batch_size,
                                         shuffle=True,
                                         drop_last=True,
                                         num_workers=args.num_workers)

    test_loader = data_utils.DataLoader(val_dataset,
                                        batch_size=args.batch_size,
                                        shuffle=True,
                                        num_workers=args.num_workers)

    model = SigNet(input_shape=(1,) + args.input_size, num_classes=len(le.classes_))
    model.to(args.device)

    # optimizer = optim.SGD(model.parameters(),
    #                       lr=args.lr,
    #                       momentum=0.9,
    #                       weight_decay=1e-5)
    optimizer = optim.SGD(model.parameters(),
                          lr=args.lr,
                          momentum=args.momentum,
                          nesterov=True,
                          weight_decay=args.weight_decay,
                          )

    scheduler = StepLR(optimizer, step_size=max(1, args.epochs // args.lr_decay_times),
                       gamma=args.lr_decay)

    best_loss = float('inf')
    logs = {'train_loss': [],
            'test_loss': [],
            'train_cls_acc': [],
            'train_forg_acc': [],
            'test_cls_acc': [],
            'test_forg_acc': []
            }
    for epoch in range(args.epochs):
        tic = time.time()

        train_loss, train_cls_acc, train_forg_acc =\
            train_one_epoch(model,
                            train_loader,
                            optimizer,
                            epoch,
                            args.epochs,
                            args.loss_type,
                            args.lamb,
                            args.device,
                            verbose=True)

        test_loss, test_cls_acc, test_forg_acc =\
            test_one_epoch(model,
                           test_loader,
                           epoch,
                           args.epochs,
                           args.loss_type,
                           args.lamb,
                           args.device,
                           verbose=True)

        logs['train_loss'].append(train_loss)
        logs['test_loss'].append(test_loss)
        logs['train_cls_acc'].append(train_cls_acc)
        logs['train_forg_acc'].append(train_forg_acc)
        logs['test_cls_acc'].append(test_cls_acc)
        logs['test_forg_acc'].append(test_forg_acc)

        print('Epoch [%d/ %d]' % (epoch+1, args.epochs))
        print('Train Loss: %1.4f' % train_loss)
        print('Val Loss: %1.4f' % test_loss)
        print('Train cls acc: %1.4f' % train_cls_acc)
        print('Test cls acc: %1.4f' % test_cls_acc)
        print('Train forg acc: %1.4f' % train_forg_acc)
        print('Test forg acc: %1.4f' % test_forg_acc)
        print('Time: %d' % (time.time() - tic))
        print(' '*40)

        is_best = test_loss < best_loss
        best_loss = min(best_loss, test_loss)

        save_checkpoint({'epoch': epoch, 'state_dict': model.state_dict(),
                         'test_loss': test_loss, 'is_best': is_best,
                         'batch_size': args.batch_size,
                         'cls_acc': test_cls_acc, 'forg_acc': test_forg_acc},
                        checkpoint_path="checkpoints_f/checkpoint.pth.tar")
        scheduler.step()
    return logs


if __name__ == '__main__':

    class Args:
        dataset = 'cedar'
        data_dir = 'signatures/'
        train_proportion = 0.7
        batch_size = 8
        epochs = 20
        num_workers = 4
        input_size = (150, 220)
        loss_type = 'L1'
        lamb = 0.95

        # Optimizer
        lr = 0.001
        lr_decay = 0.1
        lr_decay_times = 3
        momentum = 0.9
        weight_decay = 1e-4

        height = 155
        width = 220
        seed = 0
        device = torch.device("cuda:0") if torch.cuda.is_available()\
            else torch.device("cpu")

    args = Args()
    logs = main(args)



