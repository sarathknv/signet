import os
import random
import time

import cv2
import numpy as np
import shutil
import torch
import torch.nn as nn
from torch import optim
from torch.optim.lr_scheduler import StepLR
from torch.utils.data import Dataset
import torch.utils.data as data_utils
from sklearn import preprocessing
from torchvision import transforms
from skimage import filters, transform
from wi import SigNet, SignatureDataset
from typing import Tuple, Optional, Dict
from tqdm import tqdm
import sklearn
import sklearn.svm
import sklearn.pipeline as pipeline
import metrics


def create_training_set_for_user(user: int,
                                 exp_train: Tuple[np.ndarray, np.ndarray, np.ndarray],
                                 num_forg_from_exp: int,
                                 num_forg_train,
                                 other_negatives: np.ndarray,
                                 rng: np.random.RandomState) -> Tuple[np.ndarray, np.ndarray]:
    """ Creates a training set for training a WD classifier for a user
    Parameters
    ----------
    user: int
        The user for which a dataset will be created
    exp_train: tuple of np.ndarray (x, y, yforg)
        The training set split of the exploitation dataset
    num_forg_from_exp: int
        The number of random forgeries from each user in the exploitation set
        (other than "user") that will be used as negatives
    other_negatives: np.ndarray
        A collection of other negative samples (e.g. from a development set)
    rng: np.random.RandomState
        The random number generator (for reproducibility)
    Returns
    -------
    np.ndarray (N), np.ndarray (N)
        The dataset for the user (x, y), where N is the number of signatures
        (genuine + random forgeries)
    """
    exp_x, exp_y, exp_yforg = exp_train

    positive_samples = exp_x[(exp_y == user) & (exp_yforg == 0)]
    idx = np.flatnonzero((exp_y == user) & (exp_yforg == 1))
    chosen_idx = rng.choice(idx, num_forg_train, replace=False)
    negative_samples_skilled = exp_x[chosen_idx]

    if num_forg_from_exp > 0:
        users = np.unique(exp_y)
        other_users = list(set(users).difference({user}))
        negative_samples_from_exp = []
        for other_user in other_users:
            idx = np.flatnonzero((exp_y == other_user) & (exp_yforg == False))
            chosen_idx = rng.choice(idx, num_forg_from_exp, replace=False)
            negative_samples_from_exp.append(exp_x[chosen_idx])
        negative_samples_from_exp = np.concatenate(negative_samples_from_exp)
    else:
        negative_samples_from_exp = []
    negative_samples_from_exp = np.concatenate((negative_samples_from_exp, negative_samples_skilled))
    if len(other_negatives) > 0 and len(negative_samples_from_exp) > 0:
        negative_samples = np.concatenate((negative_samples_from_exp, other_negatives))
    elif len(other_negatives) > 0:
        negative_samples = other_negatives
    elif len(negative_samples_from_exp) > 0:
        negative_samples = negative_samples_from_exp
    else:
        raise ValueError('Either random forgeries from exploitation or from development sets must be used')

    train_x = np.concatenate((positive_samples, negative_samples))
    train_y = np.concatenate((np.full(len(positive_samples), 1),
                              np.full(len(negative_samples), -1)))
    return train_x, train_y


def train_wdclassifier_user(training_set: Tuple[np.ndarray, np.ndarray],
                            svmType: str,
                            C: float,
                            gamma: Optional[float]) -> sklearn.svm.SVC:
    """ Trains an SVM classifier for a user
    Parameters
    ----------
    training_set: Tuple (x, y)
        The training set (features and labels). y should have labels -1 and 1
    svmType: string ('linear' or 'rbf')
        The SVM type
    C: float
        Regularization for the SVM optimization
    gamma: float
        Hyperparameter for the RBF kernel
    Returns
    -------
    sklearn.svm.SVC:
        The learned classifier
    """

    assert svmType in ['linear', 'rbf']

    train_x = training_set[0]
    train_y = training_set[1]

    # Adjust for the skew between positive and negative classes
    n_genuine = len([x for x in train_y if x == 1])
    n_forg = len([x for x in train_y if x == -1])
    skew = n_forg / float(n_genuine)

    # Train the model
    if svmType == 'rbf':
        model = sklearn.svm.SVC(C=C, gamma=gamma, class_weight={1: skew})
    else:
        model = sklearn.svm.SVC(kernel='linear', C=C, class_weight={1: skew})

    model_with_scaler = pipeline.Pipeline([('scaler', preprocessing.StandardScaler(with_mean=False)),
                                           ('classifier', model)])

    model_with_scaler.fit(train_x, train_y)

    return model_with_scaler


def train_all_users(exp_train,
                    svm_type: str,
                    C: float,
                    gamma: float,
                    num_forg_from_exp: int,
                    num_forg_train,
                    rng: np.random.RandomState):
    """ Train classifiers for all users in the exploitation set
    Parameters
    ----------
    exp_train: tuple of np.ndarray (x, y, yforg)
        The training set split of the exploitation set (system users)
    dev_set: tuple of np.ndarray (x, y, yforg)
        The development set
    svm_type: string ('linear' or 'rbf')
        The SVM type
    C: float
        Regularization for the SVM optimization
    gamma: float
        Hyperparameter for the RBF kernel
    num_forg_from_dev: int
        Number of forgeries from each user in the development set to
        consider as negative samples
    num_forg_from_exp: int
        Number of forgeries from each user in the exploitation set (other
        than the current user) to consider as negative sample.
    rng: np.random.RandomState
        The random number generator (for reproducibility)
    Returns
    -------
    Dict int -> sklearn.svm.SVC
        A dictionary of trained classifiers, where the keys are the users.
    """
    classifiers = {}

    exp_y = exp_train[1]
    users = np.unique(exp_y)

    other_negatives = []

    for user in tqdm(users):
        training_set = create_training_set_for_user(user, exp_train, num_forg_from_exp, num_forg_train, other_negatives, rng)
        classifiers[user] = train_wdclassifier_user(training_set, svm_type, C, gamma)

    return classifiers


def test_all_users(classifier_all_user: Dict[int, sklearn.svm.SVC],
                   exp_test: Tuple[np.ndarray, np.ndarray, np.ndarray],
                   global_threshold: float) -> Dict:
    """ Test classifiers for all users and return the metrics
    Parameters
    ----------
    classifier_all_user: dict (int -> sklearn.svm.SVC)
        The trained classifiers for all users
    exp_test: tuple of np.ndarray (x, y, yforg)
        The testing set split from the exploitation set
    global_threshold: float
        The threshold used to compute false acceptance and
        false rejection rates
    Returns
    -------
    dict
        A dictionary containing a variety of metrics, including
        false acceptance and rejection rates, equal error rates
    """
    xfeatures_test, y_test, yforg_test = exp_test

    genuinePreds = []
    randomPreds = []
    skilledPreds = []

    users = np.unique(y_test)
    for user in users:
        model = classifier_all_user[user]

        # Test the performance for the user without replicates
        skilled_forgeries = xfeatures_test[(y_test == user) & (yforg_test == 1)]
        test_genuine = xfeatures_test[(y_test == user) & (yforg_test == 0)]
        random_forgeries = xfeatures_test[(y_test != user) & (yforg_test == 0)]

        genuinePredUser = model.decision_function(test_genuine)
        skilledPredUser = model.decision_function(skilled_forgeries)
        randomPredUser = model.decision_function(random_forgeries)

        genuinePreds.append(genuinePredUser)
        skilledPreds.append(skilledPredUser)
        randomPreds.append(randomPredUser)

    # Calculate al metrics (EER, FAR, FRR and AUC)
    all_metrics = metrics.compute_metrics(genuinePreds, randomPreds, skilledPreds, global_threshold)

    results = {'all_metrics': all_metrics}
               # 'predictions': {'genuinePreds': genuinePreds,
               #                 'randomPreds': randomPreds,
               #                 'skilledPreds': skilledPreds}}

    # print(all_metrics['EER'], all_metrics['EER_userthresholds'])
    return results


# Data utilities
def split_train_test(exp_set,
                     num_gen_train,
                     num_gen_test,
                     num_forg_train,
                     num_forg_test,
                     rng=np.random.RandomState()):
    """ Splits a set into training and testing. Both sets contains the same users. The
        training set contains only genuine signatures, while the testing set contains
        genuine signatures and forgeries. Note that the number of genuine signatures for
        training plus the number of genuine signatures for test must be smaller or equal to
        the total number of genuine signatures (to ensure no overlap)
    Parameters
    ----------
    exp_set: tuple of np.ndarray (x, y, yforg)
        The dataset
    num_gen_train: int
        The number of genuine signatures to be used for training
    num_gen_test: int
        The number of genuine signatures to be used for testing
    rng: np.random.RandomState
        The random number generator (for reproducibility)
    Returns
    -------
    tuple of np.ndarray (x, y, yforg)
        The training set
    tuple of np.ndarray (x, y, yforg)
        The testing set
    """
    x, y, yforg = exp_set
    users = np.unique(y)

    train_idx = []
    test_idx = []

    for user in users:
        user_genuines = np.flatnonzero((y == user) & (yforg == False))
        rng.shuffle(user_genuines)
        user_train_idx = user_genuines[0:num_gen_train]
        user_test_idx = user_genuines[-num_gen_test:]

        # Sanity check to ensure training samples are not used in test:
        assert len(set(user_train_idx).intersection(user_test_idx)) == 0

        train_idx += user_train_idx.tolist()
        test_idx += user_test_idx.tolist()

        user_forgeries = np.flatnonzero((y == user) & (yforg == True))

        user_train_idx = user_forgeries[0:num_forg_train]
        user_test_idx = user_forgeries[-num_forg_test:]

        assert len(set(user_train_idx).intersection(user_test_idx)) == 0
        train_idx += user_train_idx.tolist()
        test_idx += user_test_idx.tolist()

        # test_idx += user_forgeries.tolist()

    exp_train = x[train_idx], y[train_idx], yforg[train_idx]
    exp_test = x[test_idx], y[test_idx], yforg[test_idx]

    return exp_train, exp_test


def get_train_test_subject_ids(total_subjects=50, development_proportion=0.7, exploitation_proportion=0.2,
                               validation_proportion=0.1):
    assert development_proportion + exploitation_proportion + validation_proportion <= 1

    num_development = int(total_subjects * development_proportion)
    num_exploitation = int(total_subjects * exploitation_proportion)
    num_validation = int(total_subjects * validation_proportion)
    total_ids = np.arange(1, total_subjects + 1, dtype=int)
    np.random.shuffle(total_ids)

    development_ids = total_ids[:num_development].copy()
    exploitation_ids = total_ids[num_development: num_development + num_exploitation].copy()
    validation_ids = total_ids[num_development + num_exploitation: num_development + num_exploitation + num_validation].copy()
    return development_ids, exploitation_ids, validation_ids


def create_exploitation_dataset(data_dir, subject_ids, num_gen=24, num_forg=24):
    le = preprocessing.LabelEncoder()
    le.fit(subject_ids)
    samples = []

    for idx in subject_ids:
        for i in range(1, num_gen+1):
            path = os.path.join(data_dir,
                                'full_org',
                                'original_%d_%d.png' % (idx, i))
            sample = {'path': path,
                      'label': le.transform([idx])[0],
                      'is_forgery': False
                      }
            samples.append(sample)

        for j in range(1, num_forg + 1):
            path = os.path.join(data_dir,
                                'full_forg',
                                'forgeries_%d_%d.png' % (idx, j))

            sample = {'path': path,
                      'label': le.transform([idx])[0],
                      'is_forgery': True
                      }
            samples.append(sample)

    # random.shuffle(samples)
    return samples, le


def get_embeddings(dataset, model, device):
    all_x = []
    all_y = []
    all_y_forg = []
    with torch.no_grad():
        for i, data in enumerate(dataset):
            img = data['image'].float().unsqueeze(0).to(device)
            y = data['label']
            yforg = data['is_forg']

            emb, logits, forg_logits = model(img)

            all_x.append(emb.cpu().numpy().squeeze())
            all_y.append(y)
            all_y_forg.append(yforg)
            print(i, len(dataset))

    return np.asarray(all_x), np.asarray(all_y), np.asarray(all_y_forg)


def main(args):
    np.random.seed(args.seed)

    if args.dataset.lower() == 'cedar':
        K = 55
        development_ids, exploitation_ids, validation_ids =\
            get_train_test_subject_ids(total_subjects=K)
        samples, le = create_exploitation_dataset(args.data_dir,
                                                  exploitation_ids,
                                                  num_gen=24,
                                                  num_forg=24)
    elif args.dataset.lower() == 'bengali':
        raise ValueError('Invalid dataset name')
    else:
        raise ValueError('Invalid dataset name')

    model = SigNet(input_shape=(1,) + args.input_size, num_classes=38)
    model.to(args.device)
    weights = torch.load('checkpoints_v3/best.pth.tar')
    print(weights['cls_acc'], weights['forg_acc'])
    model.load_state_dict(weights['state_dict'], strict=False)  # TODO
    model.eval()

    val_transforms = transforms.Compose([
        transforms.ToPILImage(),
        transforms.CenterCrop(args.input_size),
        transforms.ToTensor(),
    ])

    # samples = samples[:100]
    dataset = SignatureDataset(samples,
                               val_transforms)

    features, y, yforg = get_embeddings(dataset, model, args.device)
    data = (features, y, yforg)

    exp_train, exp_test = split_train_test(data, num_gen_train=12,
                                           num_gen_test=10, num_forg_train=10,
                                           num_forg_test=10)

    classifiers = train_all_users(exp_train, args.svm_type, args.C, args.gamma,
                                  num_forg_from_exp=12, num_forg_train=12, rng=np.random.RandomState())

    results = test_all_users(classifiers, exp_test, global_threshold=0)
    print(metrics)

    import pdb; pdb.set_trace()


if __name__ == '__main__':

    class Args:
        dataset = 'cedar'
        data_dir = 'signatures/'
        train_proportion = 0.7
        batch_size = 8
        epochs = 20
        num_workers = 4
        input_size = (150, 220)
        loss_type = 'L1'
        lamb = 0.95

        svm_type = 'rbf'
        C = 1
        gamma = 2**-11

        # Optimizer
        lr = 0.001
        lr_decay = 0.1
        lr_decay_times = 3
        momentum = 0.9
        weight_decay = 1e-4

        height = 155
        width = 220
        seed = 0
        device = torch.device("cuda:0") if torch.cuda.is_available()\
            else torch.device("cpu")

    args = Args()
    logs = main(args)
